package ching.tafa.globalkinetic.domain.usecase.currentWeather

import ching.tafa.globalkinetic.data.repository.WeatherRepository
import ching.tafa.globalkinetic.domain.model.Response
import ching.tafa.globalkinetic.domain.model.currentWeather.CurrentWeather
import ching.tafa.globalkinetic.domain.usecase.base.BaseUseCase

open class GetCurrentWeatherByCoordUseCase(val repository: WeatherRepository) :
    BaseUseCase<GetCurrentWeatherByCoordRequest, CurrentWeather>() {

    override suspend fun run(): Response<CurrentWeather> {
        return repository.getCurrentWeatherCoord(request!!.lon, request!!.lat)
    }
}