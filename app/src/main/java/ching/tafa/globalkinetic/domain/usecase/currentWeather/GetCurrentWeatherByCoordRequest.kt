package ching.tafa.globalkinetic.domain.usecase.currentWeather

import ching.tafa.globalkinetic.domain.usecase.base.BaseRequest

class GetCurrentWeatherByCoordRequest(val lon: Double, val lat: Double) : BaseRequest {
    override fun validate(): Boolean {
        return true
    }
}