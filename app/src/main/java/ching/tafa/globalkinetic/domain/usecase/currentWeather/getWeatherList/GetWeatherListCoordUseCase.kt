package ching.tafa.globalkinetic.domain.usecase.currentWeather.getWeatherList

import ching.tafa.globalkinetic.data.repository.WeatherRepository
import ching.tafa.globalkinetic.domain.model.Response
import ching.tafa.globalkinetic.domain.model.currentWeather.CurrentWeather
import ching.tafa.globalkinetic.domain.usecase.base.BaseUseCase
import ching.tafa.globalkinetic.domain.usecase.currentWeather.GetCurrentWeatherByCoordRequest

open class GetWeatherListCoordUseCase(val repository: WeatherRepository) :
    BaseUseCase<GetCurrentWeatherByCoordRequest, List<Response<CurrentWeather>>>() {

    override suspend fun run(): Response<List<Response<CurrentWeather>>> {
        return repository.getWeatherListCoord(request!!.lon, request!!.lat, getChannel())
    }
}