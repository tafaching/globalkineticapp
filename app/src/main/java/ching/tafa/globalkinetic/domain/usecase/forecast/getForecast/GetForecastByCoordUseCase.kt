package ching.tafa.globalkinetic.domain.usecase.forecast.getForecast

import ching.tafa.globalkinetic.data.repository.ForecastRepository
import ching.tafa.globalkinetic.domain.model.Response
import ching.tafa.globalkinetic.domain.model.forecast.Forecast
import ching.tafa.globalkinetic.domain.usecase.base.BaseUseCase

open class GetForecastByCoordUseCase(val repository: ForecastRepository) :
    BaseUseCase<GetCurrentForecastByCoordNameRequest, Forecast>() {

    override suspend fun run(): Response<Forecast> {
        return repository.getForecastByCoord(request!!)
    }
}