package ching.tafa.globalkinetic.domain.usecase.forecast.getForecast

import ching.tafa.globalkinetic.domain.usecase.base.BaseRequest

class GetCurrentForecastByCoordNameRequest(val cityName: String) : BaseRequest {
    override fun validate(): Boolean {
        return true
    }
}