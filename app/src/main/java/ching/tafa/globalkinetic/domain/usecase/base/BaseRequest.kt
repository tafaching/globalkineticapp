package ching.tafa.globalkinetic.domain.usecase.base

interface BaseRequest {
    fun validate(): Boolean
}
