package ching.tafa.globalkinetic.app.di.subcomponent.worker.weather

import dagger.Module
import ching.tafa.globalkinetic.app.worker.WeatherWorker

@Module
class WeatherWorkerModule(val weatherWorker: WeatherWorker) {

}