package ching.tafa.globalkinetic.app.di.subcomponent.forecast.fragment

import dagger.Subcomponent
import ching.tafa.globalkinetic.ui.forecast.fragment.ForecastFragment

@Subcomponent(modules = arrayOf(ForecastFragmentModule::class))
interface ForecastFragmentComponent {
    fun injectTo(fragment: ForecastFragment)
}