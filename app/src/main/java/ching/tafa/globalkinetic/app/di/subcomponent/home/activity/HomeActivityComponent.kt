package ching.tafa.globalkinetic.app.di.subcomponent.home.activity

import dagger.Subcomponent
import ching.tafa.globalkinetic.ui.home.activity.HomeActivity

@Subcomponent(modules = arrayOf(HomeActivityModule::class))
interface HomeActivityComponent {
    fun injectTo(activity: HomeActivity)
}