package ching.tafa.globalkinetic.app.di.component

import dagger.Component
import ching.tafa.globalkinetic.app.di.module.*

import ching.tafa.globalkinetic.app.di.subcomponent.forecast.activity.ForecastActivityComponent
import ching.tafa.globalkinetic.app.di.subcomponent.forecast.activity.ForecastActivityModule
import ching.tafa.globalkinetic.app.di.subcomponent.forecast.fragment.ForecastFragmentComponent
import ching.tafa.globalkinetic.app.di.subcomponent.forecast.fragment.ForecastFragmentModule
import ching.tafa.globalkinetic.app.di.subcomponent.home.activity.HomeActivityComponent
import ching.tafa.globalkinetic.app.di.subcomponent.home.activity.HomeActivityModule
import ching.tafa.globalkinetic.app.di.subcomponent.home.fragment.HomeFragmentComponent
import ching.tafa.globalkinetic.app.di.subcomponent.home.fragment.HomeFragmentModule
import ching.tafa.globalkinetic.app.di.subcomponent.worker.weather.WeatherWorkerComponent
import ching.tafa.globalkinetic.app.di.subcomponent.worker.weather.WeatherWorkerModule
import ching.tafa.globalkinetic.app.di.viewmodel.ViewModelFactoryModule
import ching.tafa.globalkinetic.app.di.viewmodel.ViewModelModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = arrayOf(
        ApplicationModule::class,
        UtilsModule::class,
        RepositoryModule::class,
        DataModule::class,
        DomainModule::class,
        ViewModelFactoryModule::class,
        ViewModelModule::class
    )
)
interface ApplicationComponent {
    /**
     * UI - ACTIVITY
     */
    fun plus(module: HomeActivityModule): HomeActivityComponent
    fun plus(module: HomeFragmentModule): HomeFragmentComponent

    // Forecast
    fun plus(module: ForecastActivityModule): ForecastActivityComponent
    fun plus(module: ForecastFragmentModule): ForecastFragmentComponent



    /**
     * WORKER
     */
    fun plus(module: WeatherWorkerModule): WeatherWorkerComponent
}