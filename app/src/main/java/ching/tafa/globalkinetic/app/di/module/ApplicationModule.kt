package ching.tafa.globalkinetic.app.di.module

import dagger.Module
import dagger.Provides
import ching.tafa.globalkinetic.app.navigator.Navigator
import ching.tafa.globalkinetic.ui.App
import javax.inject.Singleton

@Module
class ApplicationModule(val app: App) {
    @Provides @Singleton
    fun provideApp(): App = app

    @Provides @Singleton
    fun provideNavigator(): Navigator = Navigator()
}