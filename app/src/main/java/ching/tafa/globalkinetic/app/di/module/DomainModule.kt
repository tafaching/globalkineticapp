package ching.tafa.globalkinetic.app.di.module

import dagger.Module
import dagger.Provides
import ching.tafa.globalkinetic.data.repository.ForecastRepository
import ching.tafa.globalkinetic.data.repository.WeatherRepository
import ching.tafa.globalkinetic.domain.usecase.currentWeather.GetCurrentWeatherByCoordUseCase
import ching.tafa.globalkinetic.domain.usecase.currentWeather.getWeatherList.GetWeatherListCoordUseCase
import ching.tafa.globalkinetic.domain.usecase.forecast.getForecast.GetForecastByCoordUseCase
import javax.inject.Singleton


@Module
class DomainModule {

    /**
     * WEATHER
     */


    @Provides
    @Singleton
    fun provideGetWeatherListCoordUseCase(repository: WeatherRepository) =
        GetWeatherListCoordUseCase(repository)

    @Provides
    @Singleton
    fun provideGetCurrentWeatherByCoordUseCase(repository: WeatherRepository) =
        GetCurrentWeatherByCoordUseCase(repository)

    @Provides
    @Singleton
    fun provideGetForecastByCoordUseCase(repository: ForecastRepository) =
        GetForecastByCoordUseCase(repository)


}