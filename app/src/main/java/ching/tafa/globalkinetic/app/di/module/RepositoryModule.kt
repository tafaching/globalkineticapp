package ching.tafa.globalkinetic.app.di.module

import dagger.Module
import dagger.Provides
import ching.tafa.globalkinetic.data.repository.ForecastRepository
import ching.tafa.globalkinetic.data.repository.WeatherRepository
import ching.tafa.globalkinetic.data.source.disk.DiskDataSource
import ching.tafa.globalkinetic.data.source.network.INetworkDataSource
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideWeatherRepository(
        networkDataSource: INetworkDataSource,
        diskDataSource: DiskDataSource
    ) = WeatherRepository(networkDataSource, diskDataSource)


    @Provides
    @Singleton
    fun provideForecastRepository(
        networkDataSource: INetworkDataSource
    ) = ForecastRepository(networkDataSource)


}