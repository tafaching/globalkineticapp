package ching.tafa.globalkinetic.app.di.subcomponent.home.activity

import dagger.Module
import ching.tafa.globalkinetic.app.di.module.ActivityModule
import ching.tafa.globalkinetic.ui.home.activity.HomeActivity

@Module
class HomeActivityModule(activity: HomeActivity) : ActivityModule(activity) {

}