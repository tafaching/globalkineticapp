package ching.tafa.globalkinetic.app.di.subcomponent.home.fragment

import dagger.Module
import ching.tafa.globalkinetic.ui.home.fragment.HomeFragment

@Module
class HomeFragmentModule(val fragment: HomeFragment) {

}