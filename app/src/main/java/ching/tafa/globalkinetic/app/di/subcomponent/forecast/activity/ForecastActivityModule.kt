package ching.tafa.globalkinetic.app.di.subcomponent.forecast.activity

import dagger.Module
import ching.tafa.globalkinetic.app.di.module.ActivityModule
import ching.tafa.globalkinetic.ui.forecast.ForecastActivity

@Module
class ForecastActivityModule(activity: ForecastActivity) : ActivityModule(activity) {

}