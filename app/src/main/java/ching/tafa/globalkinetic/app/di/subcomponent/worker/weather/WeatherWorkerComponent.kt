package ching.tafa.globalkinetic.app.di.subcomponent.worker.weather

import dagger.Subcomponent
import ching.tafa.globalkinetic.app.worker.WeatherWorker

@Subcomponent(modules = arrayOf(WeatherWorkerModule::class))
interface WeatherWorkerComponent {
    fun injectTo(weatherWorker: WeatherWorker)
}