package ching.tafa.globalkinetic.app.di.module

import com.microhealth.lmc.utils.NetworkSystemAbstract
import dagger.Module
import dagger.Provides
import ching.tafa.globalkinetic.data.source.disk.DiskDataSource
import ching.tafa.globalkinetic.data.source.network.INetworkDataSource
import ching.tafa.globalkinetic.data.source.network.NetworkDataSource
import ching.tafa.globalkinetic.ui.App
import javax.inject.Singleton

@Module
class DataModule {

    @Provides
    @Singleton
    fun provideDiskDataSource(appContext: App) =
            DiskDataSource(appContext)

    @Provides @Singleton
    fun provideNetworkDataSource(networkSystemBase: NetworkSystemAbstract) =
            NetworkDataSource(networkSystemBase) as INetworkDataSource



}
