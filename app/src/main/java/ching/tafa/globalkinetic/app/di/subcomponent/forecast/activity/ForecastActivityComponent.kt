package ching.tafa.globalkinetic.app.di.subcomponent.forecast.activity

import dagger.Subcomponent
import ching.tafa.globalkinetic.ui.forecast.ForecastActivity

@Subcomponent(modules = arrayOf(ForecastActivityModule::class))
interface ForecastActivityComponent {
    fun injectTo(activity: ForecastActivity)
}