package ching.tafa.globalkinetic.app.di.subcomponent.forecast.fragment

import dagger.Module
import ching.tafa.globalkinetic.ui.forecast.fragment.ForecastFragment

@Module
class ForecastFragmentModule(val fragment: ForecastFragment) {

}