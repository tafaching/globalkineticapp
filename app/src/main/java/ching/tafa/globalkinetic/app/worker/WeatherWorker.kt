package ching.tafa.globalkinetic.app.worker

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import ching.tafa.globalkinetic.app.di.component.ApplicationComponent
import ching.tafa.globalkinetic.app.di.subcomponent.worker.weather.WeatherWorkerModule
import ching.tafa.globalkinetic.data.repository.WeatherRepository
import kotlinx.coroutines.Job
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext


class WeatherWorker(context: Context, params: WorkerParameters) : Worker(context, params) {
    @Inject
    lateinit var weatherRepository: WeatherRepository

    @Inject
    lateinit var coroutineContext: CoroutineContext


    private var job: Job = Job()

    fun setupInjection(applicationComponent: ApplicationComponent) {
        applicationComponent.plus(WeatherWorkerModule(this))
            .injectTo(this)
    }

    companion object {
        val TAG = WeatherWorker::class.java.simpleName
    }

    override fun doWork(): Result {
        TODO("Not yet implemented")
    }

}