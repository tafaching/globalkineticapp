package ching.tafa.globalkinetic.app.di.subcomponent.home.fragment

import dagger.Subcomponent
import ching.tafa.globalkinetic.ui.home.fragment.HomeFragment

@Subcomponent(modules = arrayOf(HomeFragmentModule::class))
interface HomeFragmentComponent {
    fun injectTo(fragment: HomeFragment)
}