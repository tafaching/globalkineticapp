package ching.tafa.globalkinetic.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ching.tafa.globalkinetic.domain.model.Response
import ching.tafa.globalkinetic.domain.model.currentWeather.CurrentWeather
import ching.tafa.globalkinetic.domain.model.currentWeather.CurrentWeatherFactory
import ching.tafa.globalkinetic.domain.usecase.currentWeather.GetCurrentWeatherByCoordRequest
import ching.tafa.globalkinetic.domain.usecase.currentWeather.GetCurrentWeatherByCoordUseCase
import ching.tafa.globalkinetic.domain.usecase.currentWeather.getWeatherList.GetWeatherListCoordUseCase
import ching.tafa.globalkinetic.ui.viewmodel.model.weather.CurrentWeatherState
import ching.tafa.globalkinetic.ui.viewmodel.model.weather.DefaultCurrentWeatherState
import ching.tafa.globalkinetic.ui.viewmodel.model.weather.ErrorCurrentWeatherState
import ching.tafa.globalkinetic.ui.viewmodel.model.weather.LoadingCurrentWeatherState
import ching.tafa.globalkinetic.utils.launchSilent
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Job
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class WeatherViewModel
@Inject
constructor(
    private val getCurrentWeatherByCoordUseCase: GetCurrentWeatherByCoordUseCase,
    private val getWeatherListCoordUseCase: GetWeatherListCoordUseCase,
    private val coroutineContext: CoroutineContext
) : ViewModel() {

    private var job: Job = Job()

    var currentWeatherStateLiveData = MutableLiveData<CurrentWeatherState>()
    var weatherListStateLiveData = MutableLiveData<List<CurrentWeatherState>>()


    fun getCityCurrentWeatherByCoord(lon: Double, lat: Double) =
        launchSilent(coroutineContext, job) {
            currentWeatherStateLiveData.postValue(
                LoadingCurrentWeatherState(
                    Response.Success(CurrentWeatherFactory.createCurrentWeatherTest())
                )
            )

            val request = GetCurrentWeatherByCoordRequest(lon, lat)
            val response = getCurrentWeatherByCoordUseCase.execute(request)
            proccessCurrentWeather(response)
        }

    private fun proccessCurrentWeather(response: Response<CurrentWeather>) {
        if (response is Response.Success) {
            currentWeatherStateLiveData.postValue(
                DefaultCurrentWeatherState(
                    response
                )
            )
        } else if (response is Response.Error) {
            currentWeatherStateLiveData.postValue(
                ErrorCurrentWeatherState(
                    response
                )
            )
        }
    }


    fun getWeatherCoordList(lon: Double, lat: Double) = launchSilent(coroutineContext, job) {
        val request = GetCurrentWeatherByCoordRequest(lon, lat);
        getWeatherListCoordUseCase.execute(request)
        processWeatherListResponse(getWeatherListCoordUseCase.getChannel().receive())
    }

    private fun processWeatherListResponse(response: Response<List<Response<CurrentWeather>>>) {
        if (response is Response.Success) {
            var weatherList = mutableListOf<CurrentWeatherState>()

            for (responseWeather in response.data) {
                if (responseWeather is Response.Success) {

                    weatherList.add(
                        DefaultCurrentWeatherState(
                            responseWeather
                        )
                    )
                }
            }
            weatherListStateLiveData.postValue(weatherList)
        } else if (response is Response.Error) {
            //weatherListStateLiveData.postValue(ErrorCurrentWeatherState(response))
        }
    }

    private val exceptionHandler = CoroutineExceptionHandler { coroutineContext, throwable ->
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}