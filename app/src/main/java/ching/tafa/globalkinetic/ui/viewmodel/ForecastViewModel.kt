package ching.tafa.globalkinetic.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ching.tafa.globalkinetic.domain.model.Response
import ching.tafa.globalkinetic.domain.model.forecast.Forecast
import ching.tafa.globalkinetic.domain.usecase.forecast.getForecast.GetCurrentForecastByCoordNameRequest
import ching.tafa.globalkinetic.domain.usecase.forecast.getForecast.GetForecastByCoordUseCase
import ching.tafa.globalkinetic.ui.viewmodel.model.forecast.ErrorGetForecastState
import ching.tafa.globalkinetic.ui.viewmodel.model.forecast.GetForecastateState
import ching.tafa.globalkinetic.ui.viewmodel.model.forecast.LoadingGetForecastState
import ching.tafa.globalkinetic.ui.viewmodel.model.forecast.SuccessGetForecastState
import ching.tafa.globalkinetic.utils.launchSilent
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Job
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class ForecastViewModel
@Inject
constructor(private val getForecastUseCase: GetForecastByCoordUseCase,
            private val coroutineContext: CoroutineContext)
    : ViewModel() {

    private var job: Job = Job()

    var mGetForecastateStateLiveData = MutableLiveData<GetForecastateState>()

    init {
    }

    fun getForecast(cityName: String) = launchSilent(coroutineContext, job) {
        mGetForecastateStateLiveData.postValue(LoadingGetForecastState())

        val request = GetCurrentForecastByCoordNameRequest(cityName)
        val response = getForecastUseCase.execute(request)
        proccessForecast(response)
    }

    private fun proccessForecast(response: Response<Forecast>){
        if (response is Response.Success) {
            mGetForecastateStateLiveData.postValue(
                SuccessGetForecastState(
                    response
                )
            )
        } else if (response is Response.Error) {
            mGetForecastateStateLiveData.postValue(
                ErrorGetForecastState(
                    response
                )
            )
        }
    }

    private val exceptionHandler = CoroutineExceptionHandler { coroutineContext, throwable ->
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}