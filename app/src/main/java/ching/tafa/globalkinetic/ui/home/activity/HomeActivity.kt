package ching.tafa.globalkinetic.ui.home.activity

import android.net.Uri
import android.os.Bundle
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import ching.tafa.globalkinetic.R
import ching.tafa.globalkinetic.app.di.component.ApplicationComponent
import ching.tafa.globalkinetic.app.di.subcomponent.home.activity.HomeActivityModule
import ching.tafa.globalkinetic.app.worker.WeatherWorker
import ching.tafa.globalkinetic.ui.base.BaseActivity
import ching.tafa.globalkinetic.ui.home.fragment.HomeFragment
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : BaseActivity(), HomeFragment.OnFragmentInteractionListener {

    override var layoutId = R.layout.activity_home

    override fun setupInjection(applicationComponent: ApplicationComponent) {
        applicationComponent.plus(HomeActivityModule(this)).injectTo(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activity_home_navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        initPeriodicWorker()
    }

    override fun onFragmentInteraction(uri: Uri) {

    }


    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                cleanFragmentBackStack()
                return@OnNavigationItemSelectedListener true
            }

        }
        false
    }


    override fun onBackPressed() {
        val homeItem = activity_home_navigation.getMenu().getItem(0)
        if (activity_home_navigation.getSelectedItemId() !== homeItem.getItemId()) {
            // Select home item
            activity_home_navigation.setSelectedItemId(homeItem.getItemId())
        } else {
            super.onBackPressed()
        }
    }

    private fun initPeriodicWorker() {
        val mWorkManager = WorkManager.getInstance()
        mWorkManager?.cancelAllWorkByTag(WeatherWorker.TAG)

        val periodicBuilder = PeriodicWorkRequest.Builder(WeatherWorker::class.java, 15, java.util.concurrent.TimeUnit.MINUTES)
        val myWork = periodicBuilder.addTag(WeatherWorker.TAG).build()
        mWorkManager?.enqueueUniquePeriodicWork(WeatherWorker.TAG, ExistingPeriodicWorkPolicy.KEEP, myWork)

   }
}
