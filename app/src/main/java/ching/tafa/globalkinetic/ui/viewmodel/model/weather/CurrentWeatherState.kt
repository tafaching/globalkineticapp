package ching.tafa.globalkinetic.ui.viewmodel.model.weather

import ching.tafa.globalkinetic.domain.model.Response
import ching.tafa.globalkinetic.domain.model.currentWeather.CurrentWeather


sealed class CurrentWeatherState {
    abstract val response: Response<CurrentWeather>
}
data class DefaultCurrentWeatherState(override val response: Response<CurrentWeather>) : CurrentWeatherState()
data class LoadingCurrentWeatherState(override val response: Response<CurrentWeather>) : CurrentWeatherState()
data class ErrorCurrentWeatherState(override val response: Response<CurrentWeather>) : CurrentWeatherState()