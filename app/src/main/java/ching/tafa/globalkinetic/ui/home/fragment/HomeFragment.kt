package ching.tafa.globalkinetic.ui.home.fragment

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.OnClick
import ching.tafa.globalkinetic.R
import ching.tafa.globalkinetic.app.di.component.ApplicationComponent
import ching.tafa.globalkinetic.app.di.subcomponent.home.fragment.HomeFragmentModule
import ching.tafa.globalkinetic.app.navigator.Navigator
import ching.tafa.globalkinetic.domain.model.Response
import ching.tafa.globalkinetic.domain.model.currentWeather.CurrentWeather
import ching.tafa.globalkinetic.ui.base.BaseFragment
import ching.tafa.globalkinetic.ui.home.fragment.adapter.HomeListRVAdapter
import ching.tafa.globalkinetic.ui.viewmodel.WeatherViewModel
import ching.tafa.globalkinetic.ui.viewmodel.model.weather.CurrentWeatherState
import ching.tafa.globalkinetic.ui.viewmodel.model.weather.DefaultCurrentWeatherState
import ching.tafa.globalkinetic.ui.viewmodel.model.weather.ErrorCurrentWeatherState
import ching.tafa.globalkinetic.ui.viewmodel.model.weather.LoadingCurrentWeatherState
import kotlinx.android.synthetic.main.fragment_home.*
import javax.inject.Inject

class HomeFragment : BaseFragment(), LocationListener {
    override var layoutId = R.layout.fragment_home

    private var listener: OnFragmentInteractionListener? = null

    // View model
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var weatherViewModel: WeatherViewModel

    private var isLoading = false

    // RV Adapter
    private var mLayoutManager: LinearLayoutManager? = null
    private var mRvAdapter: HomeListRVAdapter? = null

    @Inject
    lateinit var navigator: Navigator


    private lateinit var locationManager: LocationManager
    private val locationPermissionCode = 2

    private val LOCATION_PERMS = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION
    )

    override fun setupInjection(applicationComponent: ApplicationComponent) {
        applicationComponent.plus(HomeFragmentModule(this)).injectTo(this)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflateView(inflater, container)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        configRecyclerView()

        //Observer
        weatherViewModel =
            ViewModelProviders.of(this, viewModelFactory).get(WeatherViewModel::class.java)

        observeWeatherListViewModel()

        getLocationList()
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    fun configRecyclerView() {
        mLayoutManager = LinearLayoutManager(
            context,
            LinearLayoutManager.VERTICAL, false
        )
        fragment_home_rv.layoutManager = mLayoutManager

        mRvAdapter = HomeListRVAdapter(
            listenerAddLocationClicked = {

            },
            listenerWeatherClicked = {
                navigator.toForecastActivity(it.weather.name)
            }
        )

        fragment_home_rv.adapter = mRvAdapter
        fragment_home_swipe_refresh_rv.setOnRefreshListener {
            getLocationList()
        }
    }

    @OnClick(R.id.fragment_home_button_retry)
    fun onClickRetry() {
        getLocationList()
    }

    private fun getLocationList() {
        getLocation()
    }

    private fun observeWeatherListViewModel() {
        weatherViewModel.weatherListStateLiveData.observe(this, weatherListStateObserver)
    }


    private val weatherListStateObserver = Observer<List<CurrentWeatherState>> { weatherStateList ->
        weatherStateList?.let {
            val weatherList = mutableListOf<CurrentWeather>()
            for (weatherState in weatherStateList) {
                when (weatherState) {
                    is DefaultCurrentWeatherState -> {
                        isLoading = false
                        hideLoading()
                        hideError()
                        val success = weatherState.response as Response.Success
                        weatherList.add(success.data)
                    }
                    is LoadingCurrentWeatherState -> {
                        isLoading = true
                        showLoading()
                        hideError()
                    }
                    is ErrorCurrentWeatherState -> {
                        isLoading = false
                        hideLoading()
                        showError((it as ErrorCurrentWeatherState))
                    }
                }
            }

            showInRVWeatherList(weatherList)
        }
    }

    private fun showInRVWeatherList(weatherList: List<CurrentWeather>?) {
        mRvAdapter?.setWeatherList(weatherList)
        mRvAdapter?.notifyDataSetChanged()
    }

    private fun showLoading() {
        fragment_home_loading.visibility = View.VISIBLE
        fragment_home_swipe_refresh_rv.isRefreshing = true
    }

    private fun hideLoading() {
        fragment_home_loading.visibility = View.GONE
        fragment_home_swipe_refresh_rv.isRefreshing = false
    }

    private fun showError(errorForecastState: ErrorCurrentWeatherState) {
        fragment_home_layout_error.visibility = View.VISIBLE
        val error = errorForecastState.response as Response.Error
        fragment_home_tv_status.text = error.exception.message
    }

    private fun hideError() {
        fragment_home_layout_error.visibility = View.GONE
    }


    private fun getLocation() {
        locationManager = activity?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if ((ContextCompat.checkSelfPermission(
                activity!!,
                LOCATION_PERMS.toString()
            ) != PackageManager.PERMISSION_GRANTED)
        ) {
            ActivityCompat.requestPermissions(
                activity!!,
                LOCATION_PERMS,
                locationPermissionCode
            )
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5f, this)
    }

    override fun onLocationChanged(location: Location) {
        weatherViewModel.getWeatherCoordList(location.longitude, location.latitude)
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
        TODO("Not yet implemented")
    }



    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == locationPermissionCode) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(activity, "Permission Granted", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(activity, "Permission Denied", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
