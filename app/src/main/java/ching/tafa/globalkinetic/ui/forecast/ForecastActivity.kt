package ching.tafa.globalkinetic.ui.forecast

import android.os.Bundle
import ching.tafa.globalkinetic.app.di.component.ApplicationComponent
import ching.tafa.globalkinetic.app.di.subcomponent.forecast.activity.ForecastActivityModule
import ching.tafa.globalkinetic.ui.base.BaseBackActivity
import ching.tafa.globalkinetic.ui.forecast.fragment.ForecastFragment

class ForecastActivity : BaseBackActivity() {

    //Data
    private var cityName: String = ""

    override fun setupInjection(applicationComponent: ApplicationComponent) {
        applicationComponent.plus(ForecastActivityModule(this)).injectTo(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getExtras()
        initView()
    }

    private fun getExtras() {
        cityName = intent.extras.getString(ARG_EXTRA_CITY_NAME)
    }

    private fun initView() {
        setTitleToolbar("$cityName forecast")
        addFragmentToMainLayout(ForecastFragment.newInstance(cityName))
    }

    companion object {
        val ARG_EXTRA_CITY_NAME = "ARG_EXTRA_CITY_NAME"
    }
}
