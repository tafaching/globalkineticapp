package ching.tafa.globalkinetic.ui.forecast.fragment.adapter

import androidx.recyclerview.widget.RecyclerView
import android.text.format.DateUtils
import android.view.View
import ching.tafa.globalkinetic.BuildConfig
import ching.tafa.globalkinetic.R
import ching.tafa.globalkinetic.domain.model.forecast.Prediction
import ching.tafa.globalkinetic.ui.base.loadUrl
import kotlinx.android.synthetic.main.item_rv_forecast.view.*

class ForecastViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(prediction: Prediction, position: Int) = with(itemView) {
        item_rv_forecast_tv_date.text = DateUtils.getRelativeDateTimeString(context, prediction.dt * 1000L, DateUtils.SECOND_IN_MILLIS, DateUtils.DAY_IN_MILLIS, DateUtils.FORMAT_NO_NOON)

        // Wind
        item_rv_forecast_tv_wind.text = "${context.getString(R.string.wind)}: ${Math.round(prediction.wind.speed)} km/h"

        // Humidity
        item_rv_forecast_tv_humidity.text = "${context.getString(R.string.humidity)}: ${Math.round(prediction.main.humidity)} %"

        // TEMP BIG
        var tempRounded = Math.round(prediction.main.temp)
        item_rv_forecast_tv_temp_big.text = "${tempRounded} ºC"

        // ICON
        item_rv_forecast_iv_avatar.loadUrl("${BuildConfig.OPEN_WEATHER_URL_ICON_BASE}${prediction.weather.first().icon}.png")
    }
}