package ching.tafa.globalkinetic.data.mapper

import ching.tafa.globalkinetic.data.exception.MapperException


internal interface Mapper<in I, out O> {

    @Throws(MapperException::class)
    fun map(input: I): O
}
