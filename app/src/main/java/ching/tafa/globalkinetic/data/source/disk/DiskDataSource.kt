package ching.tafa.globalkinetic.data.source.disk

import android.app.Application
import android.content.Context
import ching.tafa.globalkinetic.data.entity.CurrentWeatherEntity


class DiskDataSource(appContext: Context) {

    companion object {
        var database: Database? = null
    }

    init {
        database = Database.createInstance(appContext as Application)
    }


    fun insertCurrentWeather(currentWeatherEntity: CurrentWeatherEntity) =
        database?.currentWeatherDao()?.insertCurrentWeather(currentWeatherEntity)


    fun getAllCurrentWeatherList() = database?.currentWeatherDao()?.getAllCurrentWeatherList()


}
