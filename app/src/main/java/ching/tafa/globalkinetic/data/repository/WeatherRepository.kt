package ching.tafa.globalkinetic.data.repository

import ching.tafa.globalkinetic.data.source.disk.DiskDataSource
import ching.tafa.globalkinetic.data.source.network.INetworkDataSource
import ching.tafa.globalkinetic.domain.model.Response
import ching.tafa.globalkinetic.domain.model.currentWeather.CurrentWeather
import kotlinx.coroutines.channels.Channel

class WeatherRepository(
    private val networkDataSource: INetworkDataSource,
    private val diskDataSource: DiskDataSource
) {

    val TAG = WeatherRepository::class.java.simpleName


    suspend fun getCurrentWeatherCoord(lon: Double, lat: Double): Response<CurrentWeather> {
        val result = networkDataSource.getCurrentWeatherByCood(lon, lat)
        // Save in database
        if (result is Response.Success) {
            val currentWeather = (result as Response.Success).data
            diskDataSource.insertCurrentWeather(currentWeather.toEntity())
        }
        return result
    }


    suspend fun getWeatherListCoord(
        lon: Double, lat: Double,
        channel: Channel<Response<List<Response<CurrentWeather>>>>
    ): Response<List<Response<CurrentWeather>>> {
        /*val result = networkDataSource.getCurrentWeatherByName(byNameRequest)
        return result*/

        // FROM DISK DATA SOURCE
        val currentWeatherLocalList = diskDataSource.getAllCurrentWeatherList()
        val list = mutableListOf<Response<CurrentWeather>>()
        if (currentWeatherLocalList != null) {
            for (currentWeatherEntity in currentWeatherLocalList) {
                list.add(Response.Success(currentWeatherEntity.toModel()))
            }
            channel.send(Response.Success(list))
        }
        // FROM REMOTE
        var weatherList = mutableListOf<Response<CurrentWeather>>()
        val result = networkDataSource.getCurrentWeatherByCood(lon, lat)
        weatherList.add(result)

        // Save in database
        if (result is Response.Success) {
            val currentWeather = (result as Response.Success).data
            diskDataSource.insertCurrentWeather(currentWeather.toEntity())
        }


        channel.send(Response.Success(weatherList))

        return Response.Success(weatherList)
    }

}