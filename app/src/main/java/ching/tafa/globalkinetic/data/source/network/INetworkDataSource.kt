package ching.tafa.globalkinetic.data.source.network

import com.microhealth.lmc.utils.NetworkSystemAbstract
import ching.tafa.globalkinetic.domain.model.Response
import ching.tafa.globalkinetic.domain.model.currentWeather.CurrentWeather
import ching.tafa.globalkinetic.domain.model.forecast.Forecast

open abstract class INetworkDataSource(private val networkSystem: NetworkSystemAbstract) {


    abstract suspend fun getCurrentWeatherByCood(lon: Double, lat: Double): Response<CurrentWeather>

    abstract suspend fun getForecastBycoord(cityName: String): Response<Forecast>

}