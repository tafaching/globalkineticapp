package ching.tafa.globalkinetic.data.repository

import ching.tafa.globalkinetic.data.source.network.INetworkDataSource
import ching.tafa.globalkinetic.domain.model.Response
import ching.tafa.globalkinetic.domain.model.forecast.Forecast
import ching.tafa.globalkinetic.domain.usecase.forecast.getForecast.GetCurrentForecastByCoordNameRequest

class ForecastRepository(
    private val networkDataSource: INetworkDataSource
) {


    suspend fun getForecastByCoord(request: GetCurrentForecastByCoordNameRequest): Response<Forecast> {
        val result = networkDataSource.getForecastBycoord(request.cityName)
        return result
    }
}