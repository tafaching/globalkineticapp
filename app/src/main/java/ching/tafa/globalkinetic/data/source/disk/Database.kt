package ching.tafa.globalkinetic.data.source.disk

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import ching.tafa.globalkinetic.data.entity.CurrentWeatherEntity

import ching.tafa.globalkinetic.data.entity.typeConverter.WeatherEntityConverter
import ching.tafa.globalkinetic.data.source.disk.dao.CurrentWeatherDao

@Database(
    entities = arrayOf(
        CurrentWeatherEntity::class

    ), version = 1
)
@TypeConverters(
    WeatherEntityConverter::class
)
abstract class Database : RoomDatabase() {


    abstract fun currentWeatherDao(): CurrentWeatherDao


    companion object {
        private val DATABASE_NAME: String = "TDDWeatherApp_db"

        fun createInstance(appContext: Application):
                ching.tafa.globalkinetic.data.source.disk.Database = Room.databaseBuilder(
            appContext,
            ching.tafa.globalkinetic.data.source.disk.Database::class.java, DATABASE_NAME
        )
            .build()
    }


}
