package ching.tafa.globalkinetic.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.whenever
import ching.tafa.globalkinetic.data.exception.NetworkConnectionException
import ching.tafa.globalkinetic.domain.model.Response
import ching.tafa.globalkinetic.domain.model.currentWeather.CurrentWeatherFactory
import ching.tafa.globalkinetic.domain.usecase.currentWeather.GetCurrentWeatherByCoordRequest
import ching.tafa.globalkinetic.domain.usecase.currentWeather.GetCurrentWeatherByCoordUseCase
import ching.tafa.globalkinetic.domain.usecase.currentWeather.getWeatherList.GetWeatherListCoordUseCase
import ching.tafa.globalkinetic.ui.viewmodel.model.weather.CurrentWeatherState
import ching.tafa.globalkinetic.ui.viewmodel.model.weather.DefaultCurrentWeatherState
import ching.tafa.globalkinetic.ui.viewmodel.model.weather.ErrorCurrentWeatherState
import ching.tafa.globalkinetic.ui.viewmodel.model.weather.LoadingCurrentWeatherState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.instanceOf
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import kotlin.coroutines.CoroutineContext


class WeatherViewModelTest {

    @get: Rule
    var rule: TestRule = InstantTaskExecutorRule()

    lateinit var viewModel: WeatherViewModel

    var coroutineContext: CoroutineContext = Dispatchers.Unconfined

    @Mock
    lateinit var getCurrentWeatherByCoordUseCase: GetCurrentWeatherByCoordUseCase

    @Mock
    lateinit var getWeatherListUseCase: GetWeatherListCoordUseCase

    @Mock
    lateinit var observer: Observer<CurrentWeatherState>

    @Mock
    lateinit var lifeCycleOwner: LifecycleOwner

    lateinit var lifeCycle: LifecycleRegistry

    private val LON = -12.3456
    private val LAT = 14.9876

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        prepareViewModel()

        lifeCycle = LifecycleRegistry(lifeCycleOwner)
        `when`(lifeCycleOwner.lifecycle).thenReturn(lifeCycle)
        lifeCycle.handleLifecycleEvent(Lifecycle.Event.ON_START)

    }


    /**
     * Verify that "getCurrentWeatherByNameUseCase" is executed once when call getCityCurrentWeather()
     */
    @Test
    fun `should request the current weather when call getCityCurrentWeather()`() {
        runBlocking {
            val response = Response.Success(CurrentWeatherFactory.createCurrentWeatherTest())
            val request = GetCurrentWeatherByCoordRequest(LON, LAT)
            whenever(getCurrentWeatherByCoordUseCase.execute(request)).thenReturn(response)

            viewModel.getCityCurrentWeatherByCoord(LON, LAT)
            Mockito.verify(getCurrentWeatherByCoordUseCase, Mockito.times(1)).execute()
        }
    }

    /**
     * Verify Loading State is set when make request to get current weather
     */
    @Test
    fun `should loading state when make request`() {
        runBlocking {
            viewModel.getCityCurrentWeatherByCoord(LON, LAT)

            assertThat(
                viewModel.currentWeatherStateLiveData.value,
                instanceOf(LoadingCurrentWeatherState::class.java)
            )
        }
    }

    /**
     * Verify when is success getting currentWeatherByName the liveData is changed with this new values
     */
    @Test
    fun `should show current weather when current weather info is received`() {
        runBlocking {
            val response = Response.Success(CurrentWeatherFactory.createCurrentWeatherTest())
            val request = GetCurrentWeatherByCoordRequest(LON, LAT)
            whenever(getCurrentWeatherByCoordUseCase.execute(request)).thenReturn(response)

            viewModel.currentWeatherStateLiveData.observe(lifeCycleOwner, observer)

            viewModel.getCityCurrentWeatherByCoord(LON, LAT)

            verify(observer).onChanged(
                DefaultCurrentWeatherState(
                    response
                )
            )
        }

    }

    /**
     * Test that checks ErrorState is set when there is no Internet connection
     */
    @Test
    fun `should show error when no Internet connection is available`() {
        runBlocking {
            val response = Response.Error(exception = NetworkConnectionException())
            val request = GetCurrentWeatherByCoordRequest(LON, LAT)
            whenever(getCurrentWeatherByCoordUseCase.execute(request)).thenReturn(response)

            viewModel.currentWeatherStateLiveData.observe(lifeCycleOwner, observer)

            viewModel.getCityCurrentWeatherByCoord(LON, LAT)

            verify(observer).onChanged(
                ErrorCurrentWeatherState(
                    response
                )
            )
        }
    }

    private fun prepareViewModel() {
        viewModel = WeatherViewModel(
            getCurrentWeatherByCoordUseCase,
            getWeatherListUseCase,
            coroutineContext
        )
    }
}