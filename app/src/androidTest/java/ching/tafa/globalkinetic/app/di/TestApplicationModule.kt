package ching.tafa.globalkinetic.app.di


import dagger.Module
import dagger.Provides
import ching.tafa.globalkinetic.app.navigator.Navigator
import ching.tafa.globalkinetic.ui.App
import ching.tafa.globalkinetic.ui.GlobalKineticApp
import javax.inject.Singleton


@Module
class TestApplicationModule(val app: GlobalKineticApp){
    @Provides @Singleton
    fun provideApp(): App = app

    @Provides @Singleton
    fun provideNavigator(): Navigator = Navigator()


}