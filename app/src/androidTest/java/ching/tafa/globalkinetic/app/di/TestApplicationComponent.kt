package ching.tafa.globalkinetic.app.di

import dagger.Component
import ching.tafa.globalkinetic.app.di.component.ApplicationComponent
import ching.tafa.globalkinetic.app.di.module.DataModule
import ching.tafa.globalkinetic.app.di.module.DomainModule
import ching.tafa.globalkinetic.app.di.module.RepositoryModule
import ching.tafa.globalkinetic.app.di.module.UtilsModule
import ching.tafa.globalkinetic.app.di.viewmodel.ViewModelFactoryModule
import ching.tafa.globalkinetic.app.di.viewmodel.ViewModelModule
import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(TestApplicationModule::class,
        UtilsModule::class,
        DomainModule::class,
        DataModule::class,
        RepositoryModule::class,
        ViewModelFactoryModule::class,
        ViewModelModule::class)
)
interface TestApplicationComponent: ApplicationComponent {
}