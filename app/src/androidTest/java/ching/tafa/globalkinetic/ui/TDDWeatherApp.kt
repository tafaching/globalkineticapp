package ching.tafa.globalkinetic.ui

import ching.tafa.globalkinetic.app.di.DaggerTestApplicationComponent
import ching.tafa.globalkinetic.app.di.TestApplicationModule

open class TDDWeatherApp : App() {

    override fun onCreate() {
        super.onCreate()
        initializeDagger()
    }

    private fun initializeDagger() {
        graph = DaggerTestApplicationComponent.builder()
                .testApplicationModule(TestApplicationModule(this))
                .build()
    }
}