# TDD Weather App
This  Android application using TDD and MVVM design pattern to show the current weather and forecast. It is written 100% in Kotlin with both UI and Unit tests 

## Languages, libraries and tools used

* [Kotlin](https://kotlinlang.org/)
* [Kotlin Coroutines (1.3)](https://github.com/Kotlin/kotlinx.coroutines)
* [Room](https://developer.android.com/topic/libraries/architecture/room.html)
* [Android Architecture Components](https://developer.android.com/topic/libraries/architecture/index.html)
* Android Support Libraries
* [Dagger 2 (2.18)](https://github.com/google/dagger)
* [Retrofit](http://square.github.io/retrofit/)
* [OkHttp](http://square.github.io/okhttp/)
* [Gson](https://github.com/google/gson)
* [Mockito](http://site.mockito.org/)
* [Espresso](https://developer.android.com/training/testing/espresso/index.html)
* [AndroidPlot](https://github.com/halfhp/androidplot)

## References

* [BufferApp](https://github.com/bufferapp/android-clean-architecture-boilerplate)
* [Android-architecture](https://github.com/dmytrodanylyk/android-architecture)



## Instructions

 1.	Pull screen to refresh your current location. 
 2.	Click the location card to view more broad forecast. 

## Screenshots

### Home

https://bitbucket.org/tafaching/globalkineticapp/src/master/images/home.jpeg

### Forecast detail

https://bitbucket.org/tafaching/globalkineticapp/src/master/images/forecast.jpeg








